'use strict';
//Version 1.0.1
import plugins       from 'gulp-load-plugins';
import yargs         from 'yargs';
import browser       from 'browser-sync';
import gulp          from 'gulp';
import rimraf        from 'rimraf';
import yaml          from 'js-yaml';
import fs            from 'fs';
import webpackStream from 'webpack-stream';
import webpack2      from 'webpack';
import named         from 'vinyl-named';

const shell = require('gulp-shell');
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;

// Load all Gulp plugins into one variable
const $ = plugins();

// Check for --production flag
const PRODUCTION = !!(yargs.argv.production);

// Load settings from settings.yml
const { COMPATIBILITY, PORT, FLASK_PORT, UNCSS_OPTIONS, PATHS } = loadConfig('config.yml');
const { FLASK_APP } = loadConfig('dynamic-variables.yml');

function loadConfig(fileName) {
    let ymlFile = fs.readFileSync(fileName, 'utf8');
    return yaml.load(ymlFile);
}
var DIST = PATHS.dist;
if (PRODUCTION){
    DIST = PATHS.prod_dist;
}
gulp.task('runserver', function (done) {
    var isWin = /^win/.test(process.platform);
    var cmd = '. venv/bin/activate && export FLASK_APP=' + FLASK_APP + ' && export FLASK_ENV=development';

    if (isWin) { //for Windows
        cmd = 'venv\\Scripts\\activate && set FLASK_APP=' + FLASK_APP + ' && set FLASK_ENV=development';
    }

    var proc = exec(cmd + ' && flask run');
    proc.stderr.on('data', function(data) {
      process.stdout.write(data);
    });

    proc.stdout.on('data', function(data) {
      process.stdout.write(data);
    });
    done();
});
// Build the "dist" folder by running all of the below tasks
gulp.task('build',
    gulp.series(clean, gulp.parallel(sass, javascript, images, copy)));

// Build the site, run the server, and watch for file changes
gulp.task('default',
    gulp.series('build', 'runserver', watch));

// Delete the "dist" folder
// This happens every time a build starts
function clean(done) {
    rimraf(DIST, done);
}

// Copy files out of the assets folder
// This task skips over the "img", "js", and "scss" folders, which are parsed separately
function copy() {
    return gulp.src(PATHS.assets)
        .pipe(gulp.dest(DIST));
}

// Compile Sass into CSS
// In production, the CSS is compressed
function sass() {
    return gulp.src('dev_assets/scss/app.scss')
        .pipe($.sourcemaps.init())
        .pipe($.sass({
            includePaths: PATHS.sass
        }).on('error', $.sass.logError))
        .pipe($.autoprefixer({
            browsers: COMPATIBILITY
        }))
        // Comment in the pipe below to run UnCSS in production
        //.pipe($.if(PRODUCTION, $.uncss(UNCSS_OPTIONS)))
        .pipe($.if(PRODUCTION, $.cleanCss({compatibility: 'ie9'})))
        .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
        .pipe(gulp.dest(DIST + '/css'))
        .pipe(browser.reload({stream: true}));
}

let webpackConfig = {
    module: {
        rules: [
            {
                test: /.js$/,
                use: [
                    {
                        loader: 'babel-loader'
                    }
                ]
            }
        ]
    }
}
// Combine JavaScript into one file
// In production, the file is minified
function javascript() {
    return gulp.src(PATHS.entries)
        .pipe(named())
        .pipe($.sourcemaps.init())
        .pipe(webpackStream(webpackConfig, webpack2))
        .pipe($.if(PRODUCTION, $.uglify()
          .on('error', e => { console.log(e); })
         ))
        .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
        .pipe(gulp.dest(DIST + '/js'));
}

// Copy images to the "dist" folder
// In production, the images are compressed
function images() {
    return gulp.src('dev_assets/img/**/*')
        .pipe($.if(PRODUCTION, $.imagemin({
          progressive: true
        })))
        .pipe(gulp.dest(DIST + '/img'));
}




// Start a server with BrowserSync to preview the site in
function server(done) {
    browser.init({
        notify: false,
        proxy: "http://127.0.0.1:" + FLASK_PORT
    });
    done();
}

// Reload the browser with BrowserSync
function reload(done) {
    browser.reload();
    done();
}

// Watch for changes to static assets, pages, Sass, and JavaScript
function watch() {
    browser.init({
        notify: false,
        proxy: "http://127.0.0.1:" + FLASK_PORT
    });
    gulp.watch(PATHS.assets, copy);
    gulp.watch('templates/**/*.html').on('all', gulp.series(browser.reload));
    gulp.watch('dev_assets/scss/**/*.scss').on('all', sass);
    gulp.watch('dev_assets/js/**/*.js').on('all', gulp.series(javascript, browser.reload));
    gulp.watch('dev_assets/img/**/*').on('all', gulp.series(images, browser.reload));
}
