from flask import Flask, render_template, redirect, url_for, flash
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Email
from wtforms.widgets import TextArea
import csv
import time
from flask_mail import Mail, Message
import os

SERVER_TYPE = os.environ.get('SERVER_TYPE', 'Not Set')

if SERVER_TYPE == 'PROD':
    app = Flask(__name__, static_folder='dist_assets')
    mail_settings = {
        "MAIL_SERVER": os.environ.get('WEBMASTER_EMAIL_HOST', ''),
        "MAIL_PORT": os.environ.get('WEBMASTER_EMAIL_PORT', ''),
        "MAIL_USE_TLS": os.environ.get('WEBMASTER_EMAIL_USE_TLS', False),
        "MAIL_USE_SSL": os.environ.get('WEBMASTER_EMAIL_USE_SSL', True),
        "MAIL_USERNAME": os.environ.get('WEBMASTER_EMAIL_HOST_USER', ''),
        "MAIL_PASSWORD": os.environ.get('WEBMASTER_EMAIL_HOST_PASSWORD', ''),
        "MAIL_DEFAULT_SENDER": os.environ.get('WEBMASTER_DEFAULT_FROM_EMAIL', ''),
    }
    app.config.update(mail_settings)
else:
    app = Flask(__name__, static_folder='tmp_assets')

app.config.update(
    DEFAULT_TO_EMAIL_ADDRESS='webmaster@pixelactions.com',  # Email to receive form submissions
    TESTING=False,
    SECRET_KEY='&!#5kc1a^dpshk_oj_uq76hw&(__l%)i2khx4x0fh#un9i=p7e'
)
mail = Mail(app)


@app.route('/')
def index():
    return render_template('index.html', page='index')

@app.route('/about.html')
def about():
    return render_template('about.html', page='about')

@app.route('/news.html')
def news():
    return render_template('news.html', page='news')


class ContactForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired(), Email("This field requires a valid email address")])
    message = StringField('message', widget=TextArea(), validators=[DataRequired()])


@app.route('/contact-us', methods=['POST', 'GET'])
def contact_us():
    form = ContactForm()
    if form.validate_on_submit():
        contact_details_list = []
        submition_date_time = time.strftime('%A %B, %d %Y %H:%M:%S')
        default_to_email = app.config["DEFAULT_TO_EMAIL_ADDRESS"]
        name = form.name
        email = form.email
        message = form.message
        contact_details_list.append(submition_date_time)
        contact_details_list.append(name.data)
        contact_details_list.append(email.data)
        contact_details_list.append(message.data)
        with open('contact_info_csv.csv', 'a', newline='') as contact_info_csv:
            if os.path.getsize('contact_info_csv.csv') == 0:
                header = csv.DictWriter(contact_info_csv, fieldnames=["Date", 'name', 'email', 'message'])
                header.writeheader()
            wr = csv.writer(contact_info_csv, delimiter=',')
            wr.writerow(contact_details_list)
            compiled_comment = "[Email:" + email.data + "]\n"
            compiled_comment += "[Message:" + message.data + "]\n\n"
            compiled_comment += "\n\nNote:Please do not use the reply button as this email was generated from the webmaster email account (and not the visitor's one).\nInstead, copy the visitor's email address from above to a newly created message."
            msg = Message(subject="A website visitor has send you the following email using the contact form:",
                          body=compiled_comment,
                          reply_to=email.data,
                          sender=email.data,
                          recipients=[default_to_email])
            mail.send(msg)
            flash('Contact form submitted successfully')
        return redirect(url_for('index'))
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Error in the %s field - %s" % (
                getattr(form, field).label.text,
                error
            ))
    return render_template('contact.html', form=form)


if __name__ == '__main__':
    app.run()
